<?php

namespace Games\HangmanBundle\EventListener;

use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;

class ResponseListener implements LoggerAwareInterface
{
    public function setLogger(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    public function onKernelResponse(FilterResponseEvent $event)
    {
        $content = $event->getResponse()->getContent();
        $request = $event->getRequest();

        $content = json_decode($content, true);
        $uuid = empty($content['uuid'])? $request->headers->get('X-Hangman-Player-uuid') : $content['uuid'];
        if (!empty($uuid)) {
            $event->getResponse()->headers->set('X-Hangman-Player-uuid', $uuid);
        }
    }
}
