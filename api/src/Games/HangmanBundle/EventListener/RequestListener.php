<?php

namespace Games\HangmanBundle\EventListener;

use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;

use Games\HangmanBundle\Exception\MissingPlayerUuidHeaderException;

class RequestListener implements LoggerAwareInterface
{
    private $allowedUris = array(
        "/v1/players" => "POST",
        "/v1/_healthCheck" => "GET"
    );

    public function setLogger(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    public function onKernelRequest(GetResponseEvent $event)
    {
        $request = $event->getRequest();
        $method = $request->getMethod();

        $uri = $request->getPathInfo();
        $isAllowed = $this->isAllowedUri($uri, $method);

        $playerUuid = $request->headers->get('X-Hangman-Player-uuid');
        $this->logger->info("X-Hangman-Player-uuid: $playerUuid, requested: $uri.");
        // check if Valid UUID
        preg_match(
            "/^[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/i",
            $playerUuid,
            $matches
        );
        if (empty($matches) && !($isAllowed)) {
            $msg = "'X-Hangman-Player-uuid' header is missing or invalid.";
            $this->logger->error($msg);
            throw new MissingPlayerUuidHeaderException($msg);
        }
    }

    protected function isAllowedUri($uri, $method)
    {
        foreach ($this->allowedUris as $allowedUri => $uriMethod) {
            if ($uri === $allowedUri && $uriMethod === $method) {
                return true;
            }
        }

        return false;
    }
}
