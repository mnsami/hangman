<?php

namespace Games\HangmanBundle\Handler;

use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerInterface;

use Doctrine\Common\Persistence\ObjectManager;
use Games\HangmanBundle\Entity\Player;
use Games\HangmanBundle\Entity\Game;

use Games\HangmanBundle\Exception\GameNotBelongToPlayerException;
use Games\HangmanBundle\Exception\LetterChosenException;
use Games\HangmanBundle\Exception\GameNotFoundExcpetion;

class GameHandler implements LoggerAwareInterface
{
    private $om;
    private $words;
    private $gameRepository;

    public function __construct(ObjectManager $objectManager, $words)
    {
        $this->setObjectManager($objectManager);
        $this->words = $words;
    }

    /**
     * {@inheritDoc}
     * @see \Psr\Log\LoggerAwareInterface::setLogger()
     */
    public function setLogger(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @param unknown $level
     * @param unknown $message
     */
    protected function log($level, $message)
    {
        if (true === isset($this->logger)) {
            $this->logger->log($level, $message);
        }
    }

    protected function gameRepository()
    {
        $this->gameRepository = $this->om->getRepository("HangmanBundle:Game");

        return $this->gameRepository;
    }

    public function setObjectManager(ObjectManager $objectManager)
    {
        $this->om = $objectManager;
    }

    public function startNewGame(Player $player)
    {
        $game = new Game();
        $game->setPlayer($player);

        $wordIdx = rand(0, count($this->words) - 1);
        $game->setWord($this->words[$wordIdx]);

        $this->om->persist($game);
        $this->om->flush();


        return $this->sanitizeGame($game);
    }

    protected function sanitizeGame(Game $game)
    {
        return array(
            'id' => $game->getId(),
            'status' => $game->getStatus(),
            'failedAttempts' => $game->getFailedAttempts(),
            'letters' => $game->getLetters(),
            'wrongLetters' => $game->getWrongTrials(),
            'score' => $game->getScore(),
            'word' => $game->sanitizeWord()
        );
    }

    public function playNextMove($gameId, $playerId, $requestContent)
    {
        $requestContent = json_decode($requestContent, true);
        $letter = $requestContent['letter'];

        $game = $this->gameRepository()->findActiveById($gameId);
        if (!$game) {
            throw new GameNotFoundExcpetion("Game not found.");
        }

        if ($game->getPlayer()->getId() != $playerId) {
            throw new GameNotBelongToPlayerException("Game does not belong to player.");
        }

        if ($game->isLetterChosen($letter)) {
            throw new LetterChosenException("Letter '$letter' is already chosen.");
        }

        if ($game->isLetterInWord($letter)) {
            $game->appendLetters($letter);
            $this->log("info", "Correct letter '$letter'.");
            if ($game->isGameWin()) {
                $game->markAsWin();
                $game->getPlayer()->setHighestScore($game->getScore());
            }
        } else {
            $game->appendWrongTrials($letter);
            $game->incrementFailedAttempts();
            $failedAttempts = $game->getFailedAttempts();
            $this->log("warning", "Incorrect letter '$letter', increasing failed_attempts to $failedAttempts.");
            if (5 == $failedAttempts) {
                $game->markAsLoose();
                $this->log("error", "Attempts reached maximum. Game Lost!");
            }
        }

        $this->om->persist($game);
        $this->om->persist($game->getPlayer());
        $this->om->flush();

        return $this->sanitizeGame($game);
    }

    public function getPlayerTopGames($playerId, $limit = null)
    {
        $games = $this->gameRepository()->getTopGamesByPlayerId($playerId, $limit);

        return $games;
    }
}
