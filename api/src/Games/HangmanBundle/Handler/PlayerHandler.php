<?php

namespace Games\HangmanBundle\Handler;

use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerInterface;

use Doctrine\Common\Persistence\ObjectManager;
use Games\HangmanBundle\Entity\Player;
use Games\HangmanBundle\Exception\DuplicateEntryException;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;

class PlayerHandler implements LoggerAwareInterface
{
    private $om;

    private $playerRepository;

    public function __construct(ObjectManager $objectManager)
    {
        $this->setObjectManager($objectManager);
    }

    /**
     * {@inheritDoc}
     * @see \Psr\Log\LoggerAwareInterface::setLogger()
     */
    public function setLogger(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    protected function playerRepository()
    {
        $this->playerRepository = $this->om->getRepository("HangmanBundle:Player");

        return $this->playerRepository;
    }

    /**
     * @param unknown $level
     * @param unknown $message
     */
    protected function log($level, $message)
    {
        if (true === isset($this->logger)) {
            $this->logger->log($level, $message);
        }
    }

    public function setObjectManager(ObjectManager $objectManager)
    {
        $this->om = $objectManager;
    }

    public function createPlayer($playerInfo)
    {
        $playerInfo = $this->serializeRequestContentToArray($playerInfo);

        $player = null;
        try {
            $player = new Player();
            $player->setUsername($playerInfo['username']);
            $player->setAge($playerInfo['age']);

            $this->om->persist($player);
            $this->om->flush();
        } catch (UniqueConstraintViolationException $e) {
            $errorMessage = $e->getPrevious()->getMessage();
            $this->log("error", $errorMessage);
            $details = $player->getDuplicateDetails($errorMessage);
            $msg = "DuplicateEntryException: failed due to '{$details[1]}', '{$details[0]}' is used before.";
            throw new DuplicateEntryException($msg);
        }

        return $player;
    }

    private function serializeRequestContentToArray($requestContent)
    {
        if (is_array($requestContent)) {
            return $requestContent;
        }

        return '' === $requestContent ? array() : json_decode($requestContent, true);
    }
}
