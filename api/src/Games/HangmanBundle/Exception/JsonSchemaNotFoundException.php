<?php

namespace Games\HangmanBundle\Exception;

class JsonSchemaNotFoundException extends Base\BaseException
{
    protected $httpStatusCode = 404;
}
