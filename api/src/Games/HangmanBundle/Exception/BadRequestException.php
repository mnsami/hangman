<?php

namespace Games\HangmanBundle\Exception;

class BadRequestException extends Base\BaseException
{
    protected $httpStatusCode = 400;
}
