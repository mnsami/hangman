<?php

namespace Games\HangmanBundle\Exception;

class GameNotFoundExcpetion extends Base\BaseException
{
    protected $httpStatusCode = 404;
}
