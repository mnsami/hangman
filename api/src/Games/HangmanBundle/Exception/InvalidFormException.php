<?php

namespace Games\HangmanBundle\Exception;

use Games\HangmanBundle\Exception\Interfaces\DetailedExceptionInterface;

class InvalidFormException extends BadRequestException implements DetailedExceptionInterface
{
    protected $message = 'Validation Failed';
    protected $extraErrors;

    public function __construct($message, array $extraErrors = array())
    {
        $this->extraErrors = $extraErrors;

        parent::__construct($message);
    }

    public function getDetailedMessage()
    {
        return $this->getErrorMessages($this->extraErrors);
    }

    protected function getErrorMessages(array $extraErrors)
    {
        $errors = array();

        foreach ($extraErrors as $key => $error) {
            $errors[$key] = $error;
        }

        return $errors;
    }
}
