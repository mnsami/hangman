<?php

namespace Games\HangmanBundle\Exception\Interfaces;

interface DetailedExceptionInterface
{
    public function getDetailedMessage();
}
