<?php

namespace Games\HangmanBundle\Exception;

class ForbiddenException extends Base\BaseException
{
    protected $httpStatusCode = 403;
}
