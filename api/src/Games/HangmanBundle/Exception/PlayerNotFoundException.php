<?php

namespace Games\HangmanBundle\Exception;

class PlayerNotFoundException extends Base\BaseException
{
    protected $httpStatusCode = 404;
    protected $message = 'Player not found.';
}
