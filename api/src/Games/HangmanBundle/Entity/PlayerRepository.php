<?php

namespace Games\HangmanBundle\Entity;

use Doctrine\ORM\EntityRepository;

class PlayerRepository extends EntityRepository
{
    public function getTopPlayers($playerId, $limit = null)
    {
        $query = $this->_em->createQuery(
            "SELECT p.username, p.highestScore FROM HangmanBundle:Player p where 
            p.id != :playerId order by p.highestScore DESC"
        );

        if (!empty($limit)) {
            $query->setMaxResults($limit);
        } else {
            $query->setMaxResults(3);
        }

        $query->setParameter('playerId', $playerId);

        return $query->getResult();
    }

    public function findPlayerByUuid($uuid)
    {
        $query = $this->_em->createQuery(
            "SELECT p FROM HangmanBundle:Player p WHERE p.uuid = :uuid"
        );

        $query->setParameter('uuid', $uuid);

        return $query->getOneOrNullResult();
    }
}
