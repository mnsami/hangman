<?php

namespace Games\HangmanBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;

/**
 *
 * @ORM\Entity
 * @ORM\Table(
 *      "players",
 *      indexes={
 *          @ORM\Index(name="player_uuid_idx", columns={"uuid"}),
 *          @ORM\Index(name="player_id_idx", columns={"id"})
 *      },
 *      uniqueConstraints={
 *          @ORM\UniqueConstraint(name="player_uuid_constraint", columns={"uuid"}),
 *          @ORM\UniqueConstraint(name="player_username_constraint", columns={"username"})
 *      }
 *  )
 *
 * @ORM\Entity(repositoryClass="Games\HangmanBundle\Entity\PlayerRepository")
 *
 * @ORM\HasLifecycleCallbacks()
 */
class Player
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="uuid", type="string", length=50)
     */
    private $uuid;

    /**
     * @ORM\Column(name="username", type="string", length=512)
     */
    private $username;

    /**
     * @ORM\Column(name="age", type="integer")
     */
    private $age;

    /**
     * @ORM\Column(name="highest_score", type="integer")
     */
    private $highestScore;

    /**
     * @ORM\Column(name="created_at", type="datetime")
     *
     */
    private $createdAt;

    /**
     * @ORM\Column(name="updated_at", type="datetime")
     *
     */
    private $updatedAt;

    public function __construct()
    {
        $this->setUuid(Uuid::uuid4()->toString());
        $this->highestScore = 0;
    }

    public function getId()
    {
        return $this->id;
    }

    private function setUuid($uuid)
    {
        $this->uuid = $uuid;
    }

    public function getUuid()
    {
        return $this->uuid;
    }

    public function setUserName($username)
    {
        $this->username = $username;
    }

    public function getUsername()
    {
        return $this->username;
    }

    public function setAge($age)
    {
        $this->age = $age;
    }

    public function getAge()
    {
        return $this->age;
    }

    /**
     * @ORM\PrePersist
     */
    public function setCreatedAtAuto()
    {
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
    }

    /**
     * @ORM\PreUpdate
     */
    public function setUpdatedAtAuto()
    {
        $this->updatedAt = new \DateTime();
    }

    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    public function setHighestScore($highestScore)
    {
        if ($highestScore > $this->highestScore) {
            $this->highestScore = $highestScore;
        }
    }

    public function getHighestScore()
    {
        return $this->highestScore;
    }

    public function getDuplicateDetails($message)
    {
        $matches = array();
        preg_match_all("/\'(.*?)\'/", $message, $matches);

        return $matches[1];
    }
}
