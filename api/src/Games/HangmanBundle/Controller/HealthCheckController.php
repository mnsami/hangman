<?php

namespace Games\HangmanBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\View\View;

class HealthCheckController extends FOSRestController
{
    /**
     * @Get("/_healthCheck")
     */
    public function aliveAction()
    {
        return View::create(array('status' => 'i am ok'), 200);
    }
}
