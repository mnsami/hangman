<?php

namespace Games\HangmanBundle\Controller;

use Symfony\Component\HttpFoundation\Request;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\View\View;

use Games\HangmanBundle\Exception\InvalidFormException;
use Games\HangmanBundle\Exception\DuplicateEntryException;
use Games\HangmanBundle\Exception\PlayerNotFoundException;

class PlayersController extends FOSRestController
{
    /**
     * @Post("/players", defaults={"action" = "create_player"})
     */
    public function createPlayerAction(Request $request, $action)
    {
        $requestContent = $request->getContent();

        $this->get('hangman.service.json_form_validator')->assertValidJobSchema($requestContent, $action);
        $this->get('logger')->info('Creating new player: ' . json_encode($requestContent));
        $response['player'] = $this->get('hangman.handler.player')->createPlayer($requestContent);
        $response['code'] = 200;

        return View::create(
            empty($response['player'])? $response : $response['player'],
            $response['code']
        );
    }

    /**
     * @Get("/players/{uuid}")
     */
    public function getPlayerByUuidAction(Request $request, $uuid)
    {
        $player = $this->get('doctrine')->getRepository('HangmanBundle:Player')->findPlayerByUuid($uuid);

        if (!$player) {
            throw new PlayerNotFoundException();
        }

        return View::create($player, 200);
    }

    /**
     * @Get("/players")
     */
    public function getPlayersAction(Request $request)
    {
        $playerUuid = $request->headers->get('X-Hangman-Player-uuid');
        $player = $this->get('doctrine')->getRepository('HangmanBundle:Player')->findPlayerByUuid($playerUuid);

        if (!$player) {
            throw new PlayerNotFoundException();
        }

        $players = $this->get('doctrine')->getRepository('HangmanBundle:Player')->getTopPlayers($player->getId());

        return View::create($players, 200);
    }
}
