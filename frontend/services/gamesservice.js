'use strict';

hangmanApp.service('gamesService', ['$http', 'playersService', 'hangman_api_player_header', 'hangman_api_url', function($http, playersService, hangmanApiPlayerHeader, hangmanApiUri){
    var service = {
        // information about currentUser
        activeGame: null,

        // register new user in the backend
        startNewGame: function() {
            var registrationUrl = hangmanApiUri + '/games';

            var player = playersService.requestCurrentUser();
            if (!player) {
                throw "You are not registered.";
            }

            var requestHeaders = {
                headers: {
                    'Content-Type': 'application/json'
                }
            };
            requestHeaders.headers[hangmanApiPlayerHeader] = player.uuid;

            var request = $http.post(registrationUrl, {}, requestHeaders);

            return request.then(function(response) {
                service.activeGame = response.data;
                localStorage.activeGame = angular.toJson(service.activeGame);
                return service.activeGame;
            });
        },

        playTurn: function (char, gameId) {
            var playTurnUrl = hangmanApiUri + '/games/' + gameId + '/next';

            var player = playersService.requestCurrentUser();
            if (!player) {
                throw "You are not registered.";
            }

            var requestHeaders = {
                headers: {
                    'Content-Type': 'application/json'
                }
            };
            requestHeaders.headers[hangmanApiPlayerHeader] = player.uuid;

            var request = $http.post(playTurnUrl, {letter: char}, requestHeaders);

            return request.then(function(response) {
                service.activeGame = response.data;
                localStorage.activeGame = angular.toJson(service.activeGame);
                return service.activeGame;
            });
        },

        // get active game
        getActiveGame: function() {
            if (!service.activeGame) {
                // try fetch it from localStorage
                service.activeGame = angular.fromJson(localStorage.activeGame);
            }

            return service.activeGame;
        },
    };

    return service;
}]);
