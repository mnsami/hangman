'use strict';

hangmanApp.component('navbar', {
  templateUrl: 'components/navbar/navbar.html',
  controller: function(playersService) {
    var $ctrl = this;

    this.getRegisterLink = function () {
      return 'Register';
    }

    this.getGamesLink = function () {
      return 'Games';
    }

    this.isAuthenticated = function() {
      return playersService.isAuthenticated();
    };
  }
});
